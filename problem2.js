const fs = require('fs/promises');
const path = require('path');

const writeFile = (fileName, data) => {
    return fs.writeFile(path.join(__dirname, fileName), data);
}

const appendFile = (fileName, data) => {
    return fs.appendFile(path.join(__dirname, fileName), ` ${data}`);
}

const deleteFile = (fileName) => {
    return fs.unlink(path.join(__dirname, fileName));
}

const readFile = (fileName) => {
    return fs.readFile(path.join(__dirname, fileName))
        .then((data) => {
            return Promise.resolve(data);
        })
}

const sortData = (data) => {
    data = data.split(" ");
    return data.sort((wordA, wordB) => {
        return wordA.localeCompare(wordB);
    })
}

const problem2 = (newFile) => {
    // 1. Read lipsum.txt file
    readFile("lipsum.txt")
        .then((data) => {
            return data.toString();
        })

        // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        .then((fileData) => {
            return fileData.toLocaleUpperCase();
        })
        .then((upperCaseData) => {
            let fileName = "upper.txt"
            return writeFile(fileName, upperCaseData)
                .then(() => {
                    console.log("upper.txt create");
                    return Promise.resolve(fileName)
                })
        })
        .then((upper) => {
            return writeFile(newFile, upper)
                .then(() => {
                    console.log(`${upper} file name write into ${newFile}`);
                    return Promise.resolve(upper)
                })
        })

        // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        .then((upper) => {
            return readFile(upper);
        })
        .then((data) => {
            return data.toString().toLowerCase().split('. ');
        })
        .then((lowerData) => {
            let fileName = "lower.txt";
            return writeFile(fileName, `${lowerData}`)
                .then(() => {
                    console.log(`${fileName} is created...`);
                    return fileName;
                })
        })
        .then((fileName) => {
            return appendFile(newFile, fileName)
                .then(() => {
                    console.log(`${fileName} file is append into ${newFile}`);
                    return fileName;
                });
        })

        // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        .then((lower) => {
            return readFile(lower);      
        })
        .then((data) => {
            return data.toString();
        })
        .then((lowerData) => {
            return sortData(lowerData);
        })
        .then((sortedData) => {
            console.log("data sorted....");
            let fileName = "sortedData.txt";

            return writeFile(fileName, `${sortedData}`)

                .then(() => {
                    console.log(`${fileName} is created...`);
                    return fileName;
                })

        })
        .then((fileName) => {
            return appendFile(newFile, fileName)
                .then(() => {
                    console.log(`${fileName} file is append into ${newFile}`);
                    return newFile;
                });
        })

        // 5. Read the contents of filenames.txt and delete all the new files
        .then((newFile) => {
            return readFile(newFile);
        })
        .then((data) => {
            return data.toString().split(" ");
        })
        .then((fileNameList) => {
            return fileNameList.map((file) => {
                return deleteFile(file);
            })
        })
        .then((filePromise) => {
            return Promise.allSettled(filePromise);
        })
        .then(()=>{
            console.log("file deleted..");
        })
        .catch((error)=>{
            console.log("error: ", error.message);
        })
}

module.exports = problem2;
