// create random json file and delete the file
const fs = require('fs/promises')
const path = require('path');

const makeDir = (dirName) => {
  return fs.mkdir(path.join(__dirname, dirName))
}

const createFile = (fileName, fileData) => {
  return fs.writeFile(path.join(__dirname, fileName), fileData)
}

const deleteFile = (fileName) => {
  return fs.unlink(path.join(__dirname, fileName), fileName)
}


const problem1 = () => {

  makeDir("allFiles")
    .then(() => {
      const files = Array(10).fill(0)
        .map(() => {
          return (`${(Math.random() * 100).toFixed()}.json`)
        });

      let filePromises = files.map((file) => {
        return createFile(`allFiles/${file}`, "this is file")
      });

      return Promise.allSettled(filePromises)
        .then(() => {
          console.log("files created..");
          return Promise.resolve(files)
        })
    })

    .then((fileList) => {
      fileListPromise = fileList.map((file) => {
        return deleteFile(`allFiles/${file}`)
      });
      return Promise.allSettled(fileListPromise);
    })

    .then(() => {
      console.log("files deleted..");
    })
    .catch((error) => {
      console.log("error: ", error.message);
    })

}

module.exports = problem1;
